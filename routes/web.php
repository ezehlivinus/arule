<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('pages.home');
//});
//
//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/', function() {
//    return view('pages.home');
//});
//
////
//Route::get('about', function()
//{
//
//    return View::make('pages.contact');
//
//});
//
//

//Route::get('/', ['as' => 'index', 'uses' => 'RulesController@index']);
//Route::get('/data-sets/upload', ['as' => 'upload', 'uses' => 'RulesController@index']);


Route::group(['as' => 'arules.', 'prefix' => '/'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'RulesController@index']);
    Route::get('/data-sets/import', ['as' => 'import', 'uses' => 'DatasetController@index']);
    Route::post('/data-sets/send', ['as' => 'import_data', 'uses' => 'DatasetController@importDataSet']);
    Route::get('/data-sets/list', ['as' => 'list_data', 'uses' => 'DatasetController@listDatasets']);
    Route::get('/data-sets/sample-one', ['as' => 'sample_one', 'uses' => 'DatasetController@sampleOneAssociation']);
    Route::get('/data-sets/sample-two', ['as' => 'sample_two', 'uses' => 'DatasetController@sampleTwoAssociation']);

    Route::get('/data-sets/{slug}', ['as' => 'predict', 'uses' => 'DatasetController@predictAssociation']);
    Route::post('/data-sets/{slug}', ['as' => 'adjust_predict', 'uses' => 'DatasetController@AdjustPredictAssociation']);


});