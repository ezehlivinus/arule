let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');


// mix.scripts([
//     '../../node_modules/jquery/dist/jquery.js'
// ], 'public/js/all.js');
//
// mix.styles([
//     'public/css/vendor/normalize.css',
//     'public/css/vendor/videojs.css'
// ], 'public/css/all.css');

// mix.css('resources/assets/js/app.js', 'public/js')
//     .sass('resources/assets/sass/app.scss', 'public/css')
//     .js('resources/assets/js/bootstrap.js', 'public/js')
//     .js('../../node_modules/jquery/dist/jquery.js', 'public/js');

// mix.scripts([
//     '../../node_modules/jquery/dist/jquery.js',
// ], 'public/js/default.js', './resources/assets/');


// mix.scripts([
//     '../../node_modules/jquery/dist/jquery.js'
// ], 'public/js/main.js', './resources/assets/');