<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Phpml\Association\Apriori;
use Phpml\Dataset\CsvDataset;
use App\Dataset;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;


class DatasetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.import_datasets', compact('associator', 'samples'));
    }


    public function validateInputs(Request $request){
        $rules = array(
            //'datasettitle' => 'required',
            "file" => "mimetypes:text/csv |application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        );

        $validator = \Validator::make(Input::all(), $rules);
        if($validator->fails())
        {
            return false;
        }
        return true;
    }


    public function generateUniqueSlug($dataset_id, $title){
        $slug = str_slug($title).'-'.$dataset_id;
        $slug=Dataset::where('id', $dataset_id)
            ->update(['slug' => $slug]);
    }

    public function importDataSet(Request $request)
    {
        $title = $request->title;
        $description = $request->description;
        $dataset  = new Dataset;
        $dataset-> slug = str_slug($title);
        $dataset->title = $title;
        $dataset->description = $description;
        $dataset->save();
        $dataset_id = $dataset->id;
        $this->generateUniqueSlug($dataset_id, $title);

        if(Input::hasFile('file')){
            $file = Input::file('file');

            $uname = uniqid();
            $oname = $file->getClientOriginalName();
            $filename = $uname.$oname;
            //create a folder, data-sets, if not exist in the public folder
            $filelocation = 'data-sets';
            $filepath=$file->move($filelocation, $filename);

            ini_set('auto_detect_line_endings', TRUE);

            $header = '';
            $rows = array_map('str_getcsv', file($filepath));
            $header = array_shift($rows);
            $csv = array();
            foreach ($rows as $row) {
                $csv[] =  ['dataset_id' => $dataset_id, 'itemsets'=>json_encode($row),];
            }

            Transaction::insert($csv);
            unlink(public_path($filepath));

        }else{
            $data=[
                'status'=>false,
                'message'=>'Error occurred, choose a file!'
            ];

            return ($data);
        }
        $data=[
            'status'=>true,
            'message'=>'Datasets imported successfully!'
        ];
        return ($data);
    }

    public function AdjustPredictAssociation(Request $request, $slug)
    {
        //

        $dataset = Dataset::findBySlugOrFail($slug)
            ->with('transactions')
            ->first();
        $transactions = $dataset->transactions;

        $samples = []; $count = 0;
        foreach($transactions as $key=> $transaction){
            $samples[] = (json_decode($transaction->itemsets));
        }

        $labels = [];
        $support = $request->support/100 ?? 0.5;
        $confidence = $request->confidence/100 ?? 0.5;
        $associator = new Apriori($support, $confidence);
        $associator->train($samples, $labels);
        $associator->predict([[]]);
        //dd($associator);

        return view('pages.predict', compact('associator', 'dataset', 'samples', 'support', 'confidence'));
    }


    public function predictAssociation(Request $request, $slug)
    {

        $dataset = Dataset::findBySlugOrFail($slug)
            ->with('transactions')
            ->first();
        $transactions = $dataset->transactions;

        $samples = []; $count = 0;
        foreach($transactions as $key=> $transaction){
            $samples[] = (json_decode($transaction->itemsets));
        }

        $labels = [];
        $support = $request->support ?? 0.5;
        $confidence = $request->confidence ?? 0.5;
        $associator = new Apriori($support, $confidence);
        $associator->train($samples, $labels);
        $associator->predict([[]]);
        //dd($associator);

        return view('pages.predict', compact('associator', 'dataset', 'samples', 'support', 'confidence'));
    }

    public function listDatasets(Request $request)
    {
        //
        $datasets = Dataset::select()->paginate(5);
        return view('pages.list_datasets', compact('datasets', 'samples'));
    }

    public function sampleOneAssociation(Request $request)
    {

        //Displays sample association of item-sets
        $samples = [['alpha', 'beta', 'epsilon'], ['alpha', 'beta', 'theta'], ['alpha', 'beta', 'epsilon'], ['alpha', 'beta', 'theta']];
        $labels  = [];
        $associator = new Apriori($support = 0.5, $confidence = 0.5);
        $associator->train($samples, $labels);
        $associator->predict([[]]);
        $dataset = [
            'title'=>'Sample one data-set title',
            'description'=>'Sample one description'
        ];

        return view('pages.sample_one', compact('associator', 'dataset', 'samples'));
    }

    public function sampleTwoAssociation(Request $request)
    {

        //Displays sample association of item-sets
        $samples = [['A', 'C', 'D'], ['B', 'C', 'E'], ['A', 'B', 'C', 'E'], ['B', 'E']];
        $labels  = [];
        $associator = new Apriori($support = 0.5, $confidence = 0.5);
        $associator->train($samples, $labels);
        $associator->predict([[]]);
        $dataset = [
            'title'=>'Sample one data-set title',
            'description'=>'Sample one description'
        ];

        return view('pages.sample_two', compact('associator', 'dataset', 'samples'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
