<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\VarDumper\Cloner\Data;

class Transaction extends Model
{
    //
    protected $fillable = [
        'dataset_id',
        'itemsets'
    ];

    /**
     * Get the Dataset that own the transaction
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Dataset(){
        return $this->belongsTo(Dataset::class);
    }
}
