<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\HasSlug;

class Dataset extends Model
{
    //
    protected $fillable = [
        'slug',
        'title',
        'description'
    ];

    /**
     * Get the Transactions for the datasets
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions(){
        return $this->hasMany(Transaction::class);
    }

    public static function findBySlug($slug, $pattern = false)
    {
        if ($pattern) {
            return self::where('slug', 'LIKE', $slug);
        }

        return self::where('slug', $slug);
    }

    public static function findBySlugOrFail($slug, $pattern = false)
    {
        $builder = self::findBySlug($slug, $pattern);
        $q = clone $builder;
        if (!$q->count()) {
            abort(404);
        }
        return $builder;
    }
}
