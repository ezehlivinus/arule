@extends('layouts.default')

@section('content')

        <div  class="container">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title" style="text-align: center;">List of transactions and frequent item-sets</h4>
                <h4 class="card-subtitle mb-2 text-muted">{{$dataset['title']}}</h4>
                <h5 class="card-subtitle mb-2 text-muted">{{$dataset['description']}}</h5>
                {{--<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}
                <h6 class="card-subtitle mb-2 text-muted"></h6><br/>
                <form id="form" action="{{route('arules.import_data')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <ul class="nav nav-pills mb-3 nav-fill" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Transactions</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Database passes</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <h6 class="card-subtitle mb-2 text-muted">Each line/row is a transaction containing itemsets</h6>
                            <table class="table table-sm table-dark .table-responsive table-striped table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Item-sets</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($samples as $key=> $sample)
                                    <tr>
                                        <th scope="row">{{$loop->index}}</th>
                                        <td>{{(implode(', ',$sample))}}</td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <h6 class="card-subtitle mb-2 text-muted">Each table represent a database pass. The table contents is the results gotten after each database pass</h6>
                            @foreach($associator->large as $key=> $i)

                                #{{$loop->index+1}}
                                <table class="table table-sm table-dark .table-responsive table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th class="{{$loop->last?'bg-primary':''}}" scope="col">{{$loop->last?'The most frequent item-sets':'Frequent item-sets'}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($i as $r)

                                        <?php $most=''; ?>
                                        @foreach($r as $t)
                                            @php @$most .= @$loop->last?$t.'':$t.', ' @endphp
                                        @endforeach

                                        <tr>
                                            <th scope="row">{{$loop->index}}</th>
                                            <td>{{$most}}</td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            @endforeach
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

@endsection
@section('extra-heads')
    @parent

@endsection

@section('extra-scripts')
    @parent

    <script>

    </script>
@endsection
