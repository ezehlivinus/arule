@extends('layouts.default')

@section('content')

        <div  class="container">
        <h1 class="grey-text">List of data-sets</h1>
        <div class="row bg-white" style="{{count($datasets)?'':'display: none;'}}">
            <div class="col-md-12 col-sm-12">
                @foreach($datasets as $dataset)
                    <h4><a href="{{route("arules.predict",['slug'=>$dataset->slug]) }}">{{$dataset->title}}</a></h4>
                    <p>{{$dataset->description}}</p>
                    @endforeach
            </div>
            <div >
                {{$datasets->links()}}
            </div>
        </div>
        <div class="row" style="{{count($datasets)?'display: none;':''}} ">
            <div class="bg-white py-3 col-sm-12 col-md-12 grey-text text-lighten-1">
                No data-set uploaded
            </div>
        </div>

    </div>

@endsection
@section('extra-heads')
    @parent
<style>
    .grey-text{
        color: grey;
    }
</style>

@endsection

@section('extra-scripts')
    @parent

    <script>
        console.log($('#form'));
        var importurl = '<?= route('arules.import_data') ?>';
        $('#form').submit(function (e) {
            e.preventDefault();
            var form_data =  new FormData(this);
            console.log($('#form').serialize());
            console.log($('#file').val());
            $.ajax({
                type: 'post',
                url: importurl,
                data: form_data,
                catch: false,
                contentType: false,
                processData: false,
                success: function(xhr) {
                    //reset the form fields
                    $('#form').trigger("reset");
                    //$('#post_submitted_data').append(data);
                    // alert('Yeah!!!!');
                    //document.getElementById('#post_submitted_data').
                    //$('#post_submitted_data').trigger("reset");

                    ///{data = JSON.parse(data);}
                    //console.log(xhr);

                },
                error: function (xhr) {

                }
            });
        });
    </script>
@endsection
