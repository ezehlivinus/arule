@extends('layouts.default')

@section('content')

       <div  class="container">
        <div class="row" style="background-color: white">
            <div class="col-md-6">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ asset('images/1.jpg') }}" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Gather data</h5>
                                <p>Make the data available from the right source</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('images/2.jpg') }}" alt="Second slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Compute</h5>
                                <p>Apply association rule to the gathered data</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('images/3.jpg') }}" alt="Third slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Decision</h5>
                                <p>Make decision</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('images/4.jpg') }}" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('images/5.jpg') }}" alt="Third slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                Association rule learning is a rule-based machine learning method for discovering
                interesting relations between variables in large databases. It is intended to identify
                strong rules discovered in databases using some measures of interestingness.
                <br/><br/>
                Based on the concept of strong rules, Rakesh Agrawal, Tomasz Imieliński and
                Arun Swami[2] introduced association rules for discovering regularities between
                products in large-scale transaction data recorded by point-of-sale (POS) systems in supermarkets.
            </div>

        </div>
    </div>

@endsection
@section('extra-heads')
    @parent

    @endsection