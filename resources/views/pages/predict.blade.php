@extends('layouts.default')

@section('content')

        <div  class="container">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title" style="text-align: center;">List of transactions and frequent item-sets</h4>
                <h4 class="card-subtitle mb-2 text-muted">{{$dataset->title}}</h4>
                <h5 class="card-subtitle mb-2 text-muted">{{$dataset->description}}</h5>
                {{--<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}
                <h6 class="card-subtitle mb-2 text-muted"></h6><br/>
                <form id="form" action="{{route('arules.adjust_predict', ['slug'=>$dataset->slug])}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="title">Support (default (0.5) ): <span id="suplabel">{{$support}}</span></label>
                                <input type="range" name="support" class="form-control-range" id="support" aria-describedby="titleHelp" min="1" max="100" placeholder="Enter title">
                                <small id="supportHelp" class="form-text text-muted">Adjust the support value: <span style="font-weight: bold" id="supvalue"></span></small>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="description">Confidence (default (0.5) ): <span id="conflabel">{{$confidence}}</span></label>
                                <input type="range" name="confidence" class="form-control-range" min="1" max="100" id="confidence" aria-describedby="descriptionHelp" placeholder="Enter description">
                                <small id="confidenceHelp" class="form-text text-muted">Adjust confidence value: <span style="font-weight: bold" id="confvalue"></span></small>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <button type="submit" id="btn" class="btn btn-primary">Adjust</button>
                            </div>
                        </div>

                    </div>
                    <div class="divider"></div>

                    <ul class="nav nav-pills mb-3 nav-fill" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Transactions::: item-sets</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Database passes::: Frequent item-sets</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <h6 class="card-subtitle mb-2 text-muted">Each line/row is a transaction containing itemsets</h6>
                            <table class="table table-sm table-dark .table-responsive table-striped table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Item-sets</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($samples as $key=> $sample)
                                    <tr>
                                        <th scope="row">{{$loop->index}}</th>
                                        <td>{{(implode(', ',$sample))}}</td>
                                    </tr>
                                @empty
                                    <h2>No transaction data-sets available! Import data-set to get started</h2>
                                @endforelse

                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <h6 class="card-subtitle mb-2 text-muted">Each table represent a database pass. The table contents is the results gotten after each database pass</h6>
                            @forelse($associator->large as $key=> $i)

                                #{{$loop->index+1}}
                                <table class="table table-sm table-dark .table-responsive table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th class="{{$loop->last?'bg-primary':''}}" scope="col">{{$loop->last?'The most frequent item-sets':'Frequent item-sets'}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($i as $r)

                                        <?php $most=''; ?>
                                        @foreach($r as $t)
                                            @php @$most .= @$loop->last?$t.'':$t.', ' @endphp
                                        @endforeach

                                        <tr>
                                            <th scope="row">{{$loop->index}}</th>
                                            <td>{{$most}}</td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            @empty
                                <h2 style="text-align: center">Unable to determine the most frequent item-set(s)</h2>
                            @endforelse
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

@endsection
@section('extra-heads')
    @parent

@endsection

@section('extra-scripts')
    @parent

    <script>

        $('form #support, form #confidence').on('change click mousemove', function () {
            var support = $('#support').val();
            var confidence = $('#confidence').val();
            $('#supvalue').text(support/100);
            $('#confvalue').text(confidence/100);
        });



    </script>
@endsection
