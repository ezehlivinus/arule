@extends('layouts.default')

@section('content')

    <div  class="container">
    <div class="row">
        <div class="col-md-8 col-sm-8 offset-md-2 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Import data sets</h4>
                    <h6 class="card-subtitle mb-2 text-muted">This must be a <em>comma separated value (csv)</em> file</h6>

                    <div class="alert"></div>

                    <p class="card-text">Start by entering the title and description of the data-set.
                        This is used to identify a data-set from other data-sets, uploaded differently.</p>
                    <form id="form" action="{{route('arules.import_data')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{--<label for="title">Title</label>--}}
                                    <input type="text" name="title" class="form-control" id="title" minlength="5" required aria-describedby="titleHelp" placeholder="Enter title">
                                    <small id="titleHelp" class="form-text text-muted">This is the title of the data-set you want to import.</small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{--<label for="description">Description</label>--}}
                                    <input type="text" name="description" class="form-control" minlength="10" required id="description" aria-describedby="descriptionHelp" placeholder="Enter description">
                                    <small id="descriptionHelp" class="form-text text-muted">This is the description of the data-set you want to import.</small>
                                </div>
                            </div>

                        </div>


                        {{--<p>--}}
                        {{--@foreach($samples as $key=> $sample)--}}
                        {{--@foreach($sample as $key=> $itemsets)--}}
                        {{--{{($itemsets)}}--}}
                        {{--@endforeach--}}
                        {{--{{($sample[$key])}}<br/>--}}
                        {{--@endforeach--}}
                        {{--</p>--}}
                        {{--@foreach($associator->large as $key=> $i)--}}
                            {{--<br/>{{$loop->index}}--}}
                            {{--@foreach($i as $r)--}}
                                {{--<br/>--}}
                                {{--@foreach($r as $t)--}}
                                {{--{{$t}}--}}
                                {{--@endforeach--}}
                            {{--@endforeach--}}
                        {{--@endforeach--}}
                        <div class="form-group">
                            <label for="file">Pick the csv file to be uploaded</label>
                            <input type="file" required name="file" class="form-control-file" id="file">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

    </div>

@endsection
@section('extra-heads')
    @parent

@endsection

@section('extra-scripts')
    @parent

    <script>
        console.log($('#form'));
        $('.alert').css('display', 'none');
        var importurl = '<?= route('arules.import_data') ?>';
        $('#form').submit(function (e) {
            e.preventDefault();
            var form_data =  new FormData(this);

            $.ajax({
                type: 'post',
                url: importurl,
                data: form_data,
                catch: false,
                contentType: false,
                processData: false,
                success: function(xhr) {
                    //reset the form fields
                    if(xhr.status){
                        $('#form').trigger("reset");
                        $('.alert').removeClass('alert-danger');
                        $('.alert').addClass('alert-success');
                        $('.alert').css('display', 'block');
                        $('.alert').text(xhr.message);
                    }
                },
                error: function (xhr) {
                    $('.alert').removeClass('alert-success');
                    $('.alert').addClass('alert-danger');
                    $('.alert').css('display', 'block');
                    if(xhr.status==422){
                        $('.alert').html(xhr.responseJSON.message +":<br/> Make sure the file is csv");
                    }else{
                        $('.alert').text(xhr.message);
                    }
                }
            });
        });
    </script>
@endsection
