<meta charset="utf-8">

<meta name="description" content="">

<meta name="" content="Blade">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>Association Rule - @yield('title')</title>

<!-- load bootstrap from a cdn -->

{{--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/twitter-bootstrap/3.0.3/css/bootstrap-combined.min.css">--}}
<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
<style>
    .main{
        min-height: 70vh;
    }
    .footer{
        background-color: #343a40 !important;
        text-align: center;
        color: white;
    }
</style>
