<!doctype html>

<html>
<head>
    @include('includes.head')

   @yield('extra-heads')
</head>

<body>

    <header>
       @include('includes.header')
    </header>
<div class="py-3"></div>

    <div class="main">
      @yield('content')
    </div>

<footer class=" container-fluid footer py-3 .bg-dark"  >
    {{--include the footer definitions--}}
    @include('includes.footer')
</footer>
{{--Extra scripts--}}
@yield('extra-scripts')
</body>
</html>